function [J, grad] = costFunctionReg(theta, X, y, lambda)
%COSTFUNCTIONREG Compute cost and gradient for logistic regression with regularization
%   J = COSTFUNCTIONREG(theta, X, y, lambda) computes the cost of using
%   theta as the parameter for regularized logistic regression and the
%   gradient of the cost w.r.t. to the parameters. 

% Initialize some useful values
m = length(y); % number of training examples

% You need to return the following variables correctly 
J = 0;
grad = zeros(size(theta));

% ====================== YOUR CODE HERE ======================
% Instructions: Compute the cost of a particular choice of theta.
%               You should set J to the cost.
%               Compute the partial derivatives and set grad to the partial
%               derivatives of the cost w.r.t. each parameter in theta



H_THETA_X = sigmoid(X * theta);
Y_ONE = - ( y .* log(H_THETA_X));
Y_ZERO = - (1 - y) .* log( 1 - H_THETA_X);
BETA = H_THETA_X - y;

J = (1/m) * sum(Y_ONE + Y_ZERO);
grad = (1/m) * transpose(X) * BETA;

temp_theta = theta;
temp_theta(1) = 0;

J = J + (lambda/(2 * m)) * sum(temp_theta.^2);
grad(1, 1) = (1/m) * sum(BETA .* X(:, 1));

size(theta)
for i = 2: size(X, 2)
    grad(i, 1) = grad(i, 1) + (lambda/m) * theta(i,:);
end


% =============================================================

end
